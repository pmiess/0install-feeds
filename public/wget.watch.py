from urllib import request
import json
import re

data = request.urlopen(request.Request('https://eternallybored.org/misc/wget/releases')).read().decode('utf-8')
matches =re.findall(r'">wget-([0-9\.]+)-win64\.zip</a>\s+(....-..-..)', data)
#matches =re.findall(r'">wget-([0-9\.]+)-win(32|64)\.zip</a>\s+(....-..-..)', data)
releases = [{'version': match[0], 'released': match[1]} for match in matches]
#releases = [{'version': match[0]+'-win'+match[1], 'arch-version':match[0], 'released': match[2], 'arch':'Windows-i386' if match[1] == '32' else 'Windows-x86_64'} for match in matches]