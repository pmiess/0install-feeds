<?xml version="1.0" encoding="utf-8"?>
<interface xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://zero-install.sourceforge.net/2004/injector/interface http://0install.de/schema/injector/interface/interface.xsd http://0install.de/schema/desktop-integration/capabilities http://0install.de/schema/desktop-integration/capabilities/capabilities.xsd" uri="https://pmiess.gitlab.io/0install-feeds/ffmpeg.xml" xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>ffmpeg</name>
  <summary xml:lang="en">A complete, cross-platform solution to record, convert and stream audio and video. </summary>
  <description xml:lang="en">FFmpeg is the leading multimedia framework, able to decode, encode,  transcode, mux, demux, stream, filter and play pretty much anything that humans and machines have created. It supports the most obscure ancient formats up to the cutting edge. No matter if they were designed by some standards committee, the community or a corporation. It is also highly portable: FFmpeg compiles, runs, and passes our testing infrastructure FATE across Linux, Mac OS X, Microsoft Windows, the BSDs, Solaris, etc. under a wide variety of build environments, machine architectures, and configurations. </description>
  <icon href="https://pmiess.gitlab.io/0install-feeds/ffmpeg.ico" type="image/vnd.microsoft.icon" />
  <icon href="https://pmiess.gitlab.io/0install-feeds/ffmpeg.png" type="image/png" />
  <icon href="https://upload.wikimedia.org/wikipedia/commons/7/76/FFmpeg_icon.svg" type="image/svg" />
  <category>Video</category>
  <homepage>https://ffmpeg.org/</homepage>
  <needs-terminal />
  <group license="GPL v3 (GNU General Public License)">
    <command name="run" path="bin/ffmpeg.exe" />
    <command name="ffplay" path="bin/ffplay.exe" />
    <command name="ffprobe" path="bin/ffprobe.exe" />
    <group version="{version}" released="{released}">
      <implementation arch="Windows-x86_64" released="{released}">
        <manifest-digest/>
        <archive href="https://ffmpeg.zeranoe.com/builds/win64/shared/ffmpeg-{version}-win64-shared.zip" extract="ffmpeg-{version}-win64-shared" />
      </implementation>
      <implementation arch="Windows-i386" released="{released}">
        <manifest-digest/>
        <archive href="https://ffmpeg.zeranoe.com/builds/win32/shared/ffmpeg-{version}-win32-shared.zip" extract="ffmpeg-{version}-win32-shared" />
      </implementation>
    </group>
  </group>
  <entry-point command="run" binary-name="ffmpeg">
    <needs-terminal />
    <name xml:lang="en">ffmpeg</name>
    <summary xml:lang="en">a command line tool to convert multimedia files between formats</summary>
    <description xml:lang="en">a very fast video and audio converter that can also grab from a live audio/video source. It can also convert between arbitrary sample rates and resize video on the fly with a high quality polyphase filter. </description>
  </entry-point>
  <entry-point command="ffplay" binary-name="ffplay">
    <needs-terminal />
    <name xml:lang="en">FFplay </name>
    <summary xml:lang="en">a very simple and portable media player using the FFmpeg libraries and the SDL library. It is mostly used as a testbed for the various FFmpeg APIs. </summary>
  </entry-point>
  <entry-point command="ffprobe" binary-name="ffprobe">
    <needs-terminal />
    <name xml:lang="en">ffprobe</name>
    <summary xml:lang="en">a simple multimedia stream analyzer</summary>
    <description xml:lang="en">ffprobe gathers information from multimedia streams and prints it in human- and machine-readable fashion. </description>
  </entry-point>
</interface>
