<?xml version="1.0" ?>
<?xml-stylesheet type='text/xsl' href='interface.xsl'?>
<interface uri="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/a2ps.xml" xmlns="http://zero-install.sourceforge.net/2004/injector/interface" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://zero-install.sourceforge.net/2004/injector/interface http://0install.de/schema/injector/interface/interface.xsd http://0install.de/schema/desktop-integration/capabilities http://0install.de/schema/desktop-integration/capabilities/capabilities.xsd">
  <name>A2Ps</name>
  <summary xml:lang="en">A2ps: format files for printing on a PostScript printer</summary>
  <description xml:lang="en">GNU a2ps is an Any to PostScript filter. Of course it processes plain text files, but also pretty prints quite a few popular languages. Its slogan is precisely `` Do The Right Thing '', which means that though it is highly configurable, everything was made so that a novice user can do complicated PostScript manipulations. For instance, it has the ability to delegate the processing of some files to other filters (such as groff, texi2dvi, dvips, gzip etc.), what allows a uniform treatment (n-up, page selection, duplex etc.) of heterogeneous files. It supports a wide number of encodings, and a very good handling of Latin 2-6 should be noted, thanks to Ogonkify (by Juliusz Chroboczek). Needed fonts are automatically downloaded. The interface is internationalized, the output is customizable and there are as many options as users had wishes (table of content, headings, virtual page layout etc. etc.). 

The format used is nice and compact: normally two pages on each physical page, borders surrounding pages, headers with useful information (page number, printing date, file name or supplied header), line numbering, pretty-printing, symbol substitution etc. 
</description>
  <icon href="https://raw.githubusercontent.com/0install/0install.de-feeds/master/Gow.ico" type="image/vnd.microsoft.icon"/>
  <icon href="https://raw.githubusercontent.com/0install/0install.de-feeds/master/Gow.png" type="image/png"/>
  <category>Utility</category>
  <homepage>http://gnuwin32.sourceforge.net/packages/a2ps.htm</homepage>
  <needs-terminal/>
  <package-implementation distributions="Gentoo" package="app-text/a2ps"/>
  <implementation arch="Windows-*" id="sha1new=1290ae79d287584c2ced3e1be534b9fcef5ca4ee" langs="ca cs da de es fr it ja ko nl no pl pt ru sl sv tr" license="GPL v3 (GNU General Public License)" released="2008-03-04" version="4.14-1" version-modifier="-3">
    <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/file.xml">
      <environment insert="bin" name="PATH"/>
    </requires>
    <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/libpaper.xml">
      <environment insert="bin" name="PATH"/>
    </requires>
    <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/gettext.xml">
      <environment insert="bin" name="PATH"/>
    </requires>
    <executable-in-var name="A2PS-a2ps"/>
    <executable-in-var name="A2PS"/>
    <environment insert="etc/a2ps.cfg" mode="replace" name="A2PS_CONFIG"/>
    <environment insert="share/locale" mode="replace" name="TEXTDOMAINDIR"/>
    <command name="run" path="bin/a2ps.exe"/>
    <command name="fixnt" path="bin/fixnt.exe"/>
    <command name="sample" path="bin/sample.exe"/>
    <command name="card" path="bin/card">
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <executable-in-path command="wc" name="wc"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash"/>
    </command>
    <command name="composeglyphs" path="bin/composeglyphs">
      <runner interface="http://repo.roscidus.com/perl/perl"/>
    </command>
    <command name="fixps" path="bin/fixps">
      <environment insert="bin" name="PATH"/>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <environment insert="bin" name="PATH"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/grep.xml">
        <environment insert="bin" name="PATH"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash">
        <arg>-e</arg>
      </runner>
    </command>
    <command name="ogonkify" path="bin/ogonkify">
      <runner interface="http://repo.roscidus.com/perl/perl"/>
    </command>
    <command name="pdiff" path="bin/pdiff">
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/diffutils.xml">
        <executable-in-var name="DIFF"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <executable-in-path command="wc" name="wc"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash"/>
    </command>
    <command name="psmandup" path="bin/psmandup">
      <executable-in-path command="fixps" name="fixps"/>
      <executable-in-path command="psset" name="psset"/>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/psutils.xml">
        <executable-in-path command="psselect" name="psselect"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <executable-in-path command="expr" name="expr"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash">
        <arg>-e</arg>
      </runner>
    </command>
    <command name="psset" path="bin/psset">
      <executable-in-path command="fixps" name="fixps"/>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <executable-in-path command="cat" name="cat"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash">
        <arg>-e</arg>
      </runner>
    </command>
    <command name="texi2dvi4a2ps" path="bin/texi2dvi4a2ps">
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/mktemp.xml">
        <executable-in-path name="mktemp"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/coreutils.xml">
        <environment insert="bin" name="PATH"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/grep.xml">
        <executable-in-path command="egrep" name="egrep"/>
        <executable-in-path command="fgrep" name="fgrep"/>
      </requires>
      <requires interface="https://gitlab.com/pmiess/0install-feeds/raw/test/public/gnuwin32/sed.xml">
        <executable-in-path name="sed"/>
      </requires>
      <runner interface="http://repo.roscidus.com/utils/bash">
      </runner>
    </command>
    <manifest-digest sha256new="EAZVZPIRISQRS475C6HZAACQPD3E7RFLLVJDREY7MQYE66BUQH4Q"/>
    <archive href="https://sourceforge.net/projects/gnuwin32/files/a2ps/4.14-1/a2ps-4.14-1-bin.zip" size="1751619" type="application/zip"/>
    <archive href="https://github.com/kkeybbs/gnuwin32/blob/master/gnuwin32/a2ps-bin.zip?raw=true" size="1751619" type="application/zip"/>
  </implementation>
  <entry-point binary-name="a2ps" command="run">
    <needs-terminal/>
    <summary xml:lang="en">format files for printing on a PostScript printer</summary>
    <description xml:lang="en">Convert FILE(s) or standard  input  to  PostScript.   By
       default,  the output is sent to the default printer.  An
       output file may be specified with -o.

       Mandatory arguments to long options  are  mandatory  for
       short options too.  Long options marked with * require a
       yes/no argument, corresponding short options  stand  for
       `yes'.</description>
  </entry-point>
  <entry-point binary-name="fixnt" command="fixnt">
    <needs-terminal/>
    <summary xml:lang="en">Fixnt: format files for printing on a PostScript printer</summary>
    <description xml:lang="en">The Windows NT postscript driver has a tendency to make broken postscript files, that are incompatible with psutils. fixnt is a filter that fixes these problems, allowing the use of psnup(1). 
The filter takes the broken postscript file on stdin, and outputs a fixed postscript file on stdout. It has no other form for invocation and takes no options on the command-line.   
</description>
  </entry-point>
  <entry-point binary-name="sample" command="sample">
    <needs-terminal/>
  </entry-point>
  <entry-point binary-name="card" command="card">
    <needs-terminal/>
    <name xml:lang="en">card</name>
    <summary xml:lang="en">smartly produce a printed reference card of a program</summary>
    <description xml:lang="en">Print  a  reference card of the PROGRAMs thanks to their
       inline help.</description>
  </entry-point>
  <entry-point binary-name="composeglyphs" command="composeglyphs">
    <needs-terminal/>
    <summary xml:lang="en">Create a composite font program.</summary>
    <description xml:lang="en"> By J. Chroboczek &lt;jec at dcs.ed.ac.uk&gt;

 Copyright (c) 1996-1999 by J. Chroboczek
This code may be distributed under the terms of the 
 GNU Public License, either version 3 of the license, or (at your
 option) any later version.
</description>
  </entry-point>
  <entry-point binary-name="fixps" command="fixps">
    <needs-terminal/>
    <summary xml:lang="en">sanitize PostScript files</summary>
    <description xml:lang="en">Try  to  fix common PostScript problems that break post-
       processing.</description>
  </entry-point>
  <entry-point binary-name="ogonkify" command="ogonkify">
    <needs-terminal/>
    <summary xml:lang="en">international support for PostScript</summary>
    <description xml:lang="en">ogonkify  does  various  munging  of  PostScript   files
       related  to  printing  in different languages.  Its main
       use is to filter the  output  of  Netscape,  Mosaic  and
       other programs in order to print in languages that don't
       use the standard Western-European encoding (ISO 8859-1).</description>
  </entry-point>
  <entry-point binary-name="pdiff" command="pdiff">
    <needs-terminal/>
    <summary xml:lang="en">produce a pretty comparison between files</summary>
    <description xml:lang="en">Pretty print the differences between FILE1 and FILE2.</description>
  </entry-point>
  <entry-point binary-name="psmandup" command="psmandup">
    <needs-terminal/>
    <summary xml:lang="en">produce a version of a PS file to print in manual Duplex.</summary>
    <description xml:lang="en">Tries to produce a version of the PostScript FILE to print in manual
Duplex.
</description>
  </entry-point>
  <entry-point binary-name="psset" command="psset">
    <needs-terminal/>
    <summary xml:lang="en">Put page device definition somewhere in a PS document.</summary>
    <description xml:lang="en">Produce a version of the PostScript FILE with a protected call to the
PostScript operator setpagedevice.  Typical use is making FILE
print duplex, or on the manual tray etc.
</description>
  </entry-point>
  <entry-point binary-name="texi2dvi4a2ps" command="texi2dvi4a2ps">
    <needs-terminal/>
    <name xml:lang="en">texi2dvi</name>
    <summary xml:lang="en">produce DVI (or PDF) files from Texinfo (or LaTeX) sources.</summary>
    <description xml:lang="en">Run each Texinfo or LaTeX FILE through TeX in turn until all
cross-references are resolved, building all indices.  The directory
containing each FILE is searched for included files.  The suffix of FILE
is used to determine its language (LaTeX or Texinfo).

Makeinfo is used to perform Texinfo macro expansion before running TeX
when needed.
</description>
  </entry-point>
</interface>
