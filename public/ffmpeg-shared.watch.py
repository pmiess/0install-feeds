from urllib import request
import json
import re

excluded_versions = ['4.1', '4.0.2', '4.0.1', '4.0', '3.4.1', '3.4', '3.3.4', '3.3.3', '3.3.2', '3.3.1', '3.2.4', '3.2.2', '3.2', '3.1.5', '3.1.4']
data = request.urlopen(request.Request('https://www.videohelp.com/software/ffmpeg/old-versions#downloadold')).read().decode('utf-8')

matches =re.findall(r'ffmpeg-([0-9\.]+)-win64-static.zip</a></td><td[^>]+>(....-..-..)', data)
releases = [{'version': match[0], 'released': match[1]} for match in matches if match[0] not in excluded_versions]
