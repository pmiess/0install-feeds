from urllib import request
import string
import re
from datetime import datetime
from datetime import timedelta


data = request.urlopen(request.Request('https://busybox.net/')).read().decode('utf-8')

matches =re.findall(r'<b>([0-3]?[0-9] [a-zA-z]+ ....) -- BusyBox 1.([0-9]+).0', data)
versions = [{'version': match[1], 'released': datetime.strptime(match[0],'%d %B %Y')} for match in matches][::-1]
versions.append(	{'version': str(int(versions[-1]['version'])+1), 'released':datetime.today()+ timedelta(days=1)})


def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item): 
      return item
import string

def convert(match):
    releasedate =datetime.strptime(match[2], '%Y-%m-%d')
    releasedversion = find(lambda version: version['released'] > releasedate, versions)
    return {
        'version': '1.' + releasedversion['version'] + '.0-pre-'+ match[0],
        'released': match[2],
        'anumber':match[0],
        'hexnum':match[1]
    }

data = request.urlopen(request.Request('https://frippery.org/files/busybox/')).read().decode('utf-8')
matches =re.findall(r'busybox-w64-FRP-(....)-(g[a-f0-9]+)\.exe</a></td><td[^>]*>(....-..-..)', data)
releases = [convert(match)for match in matches if match[2]>"2019"]
